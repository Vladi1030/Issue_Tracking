CREATE DATABASE IssuesTracking;

USE IssuesTracking;

CREATE TABLE SystemUser(
	Id INT PRIMARY KEY IDENTITY,
	UserName VARCHAR(30),
	LastName VARCHAR(30),
	Dni VARCHAR(10),
	UserPassword VARCHAR(30)
);

ALTER TABLE SystemUser ADD UserType bit; 

INSERT INTO SystemUser(UserName, LastName, Dni, UserPassword, UserType) VALUES('Vladimir', 'Matarrita', '207620024', 'Vladi1030', 1); 
SELECT * FROM SystemUser;

CREATE TABLE Area(
	Id INT PRIMARY KEY IDENTITY,
	AreaName VARCHAR(50)
);

CREATE TABLE TicketState(
	Id INT PRIMARY KEY IDENTITY,
	AreaName VARCHAR(50)
);

CREATE TABLE Client(
	Id INT PRIMARY KEY IDENTITY,
	ClientName VARCHAR(50),
	Dni VARCHAR(10),
	WebSite VARCHAR(40),
	ClientAddress VARCHAR(100),
	PhoneNumber VARCHAR(12),
	Area INT,
	CONSTRAINT FK_Client_Area FOREIGN KEY (Area)     
    REFERENCES Area (Id)     
    ON DELETE CASCADE    
    ON UPDATE CASCADE
);

CREATE TABLE Contact(
	Id INT PRIMARY KEY IDENTITY,
	ContactName VARCHAR(30),
	LastName VARCHAR(30),
	Email VARCHAR(40),
	PhoneNumber VARCHAR(12),
	Position VARCHAR(20),
	IdClient INT,
	CONSTRAINT FK_Client FOREIGN KEY (IdClient)     
    REFERENCES Client (Id)     
    ON DELETE CASCADE    
    ON UPDATE CASCADE 
);

CREATE TABLE Meeting(
	Id INT PRIMARY KEY IDENTITY,
	Title VARCHAR(30),
	MeetingDate DateTime,
	IsVirtual bit,
	IdClient INT NOT NULL,
	CONSTRAINT FK_Client_Meeting FOREIGN KEY (IdClient)     
    REFERENCES Client (Id)     
    ON DELETE CASCADE    
    ON UPDATE CASCADE 
);


CREATE TABLE Ticket(
	Id INT PRIMARY KEY IDENTITY,
	Title VARCHAR(30),
	Detail VARCHAR(100),
	Reporter INT,
	Client INT,
	TicketState INT,
	CONSTRAINT FK_Client_Ticket FOREIGN KEY (Client)     
    REFERENCES Client (Id)     
    ON DELETE CASCADE    
    ON UPDATE CASCADE,
	CONSTRAINT FK_User_Ticket FOREIGN KEY (Reporter)     
    REFERENCES SystemUser (Id)     
    ON DELETE CASCADE    
    ON UPDATE CASCADE,
	CONSTRAINT FK_TicketState FOREIGN KEY (TicketState)     
    REFERENCES TicketState (Id)     
    ON DELETE CASCADE    
    ON UPDATE CASCADE 
);

SELECT * FROM Meeting;

CREATE TABLE UsersByMeeting(
	Id INT PRIMARY KEY IDENTITY,
	Meeting INT,
	AssignedUser INT,
	CONSTRAINT FK_Meeting FOREIGN KEY (Meeting)     
    REFERENCES Meeting (Id)     
    ON DELETE CASCADE    
    ON UPDATE CASCADE,
	CONSTRAINT FK_AsssignedUser FOREIGN KEY (AssignedUser)     
    REFERENCES SystemUser (Id)     
    ON DELETE CASCADE    
    ON UPDATE CASCADE 
);