﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FirstProjectAsp.Models;

namespace FirstProjectAsp.Controllers
{
    public class ClientsController : Controller
    {
        private IssuesTrackingEntities db = new IssuesTrackingEntities();

        // GET: Clients
        public ActionResult Index()
        {
            var clients = db.Clients.Include(c => c.Area1);
            if (Session["UserID"] != null)
            {
                return View(clients.ToList());
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // GET: Clients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            if (Session["UserID"] != null)
            {
                return View(client);
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // GET: Clients/Create
        public ActionResult Create()
        {
            ViewBag.Area = new SelectList(db.Areas, "Id", "AreaName");
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // POST: Clients/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ClientName,Dni,WebSite,ClientAddress,PhoneNumber,Area")] Client client)
        {
            if (ModelState.IsValid)
            {
                db.Clients.Add(client);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Area = new SelectList(db.Areas, "Id", "AreaName", client.Area);
            if (Session["UserID"] != null)
            {
                return View(client);
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // GET: Clients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            ViewBag.Area = new SelectList(db.Areas, "Id", "AreaName", client.Area);
            if (Session["UserID"] != null)
            {
                return View(client);
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // POST: Clients/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ClientName,Dni,WebSite,ClientAddress,PhoneNumber,Area")] Client client)
        {
            if (ModelState.IsValid)
            {
                db.Entry(client).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Area = new SelectList(db.Areas, "Id", "AreaName", client.Area);
            if (Session["UserID"] != null)
            {
                return View(client);
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // GET: Clients/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            if (Session["UserID"] != null)
            {
                return View(client);
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // POST: Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Client client = db.Clients.Find(id);
            db.Clients.Remove(client);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
