﻿using FirstProjectAsp.Models;
using System.Linq;
using System.Web.Mvc;

namespace IssuesTracking.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login()
        {
            if (Session["UserID"] != null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(SystemUser objUser)
        {
            if (ModelState.IsValid)
            {
                using (IssuesTrackingEntities db = new IssuesTrackingEntities())
                {
                    var obj = db.SystemUsers.Where(a => a.Dni.Equals(objUser.Dni) && a.UserPassword.Equals(objUser.UserPassword)).FirstOrDefault();
                    if (obj != null)
                    {
                        Session["UserID"] = obj.Id.ToString();
                        Session["UserName"] = obj.UserName.ToString();
                        if ((bool)obj.UserType)
                        {
                            Session["UserType"] = "admin";
                        }
                        else
                        {
                            Session["UserType"] = "basic";
                        }
                        return RedirectToAction("Index");
                    }
                }
            }
            return View(objUser);
        }

        public ActionResult Logout(SystemUser objUser)
        {
            Session.Clear();
            return RedirectToAction("Login");
        }
    }
}