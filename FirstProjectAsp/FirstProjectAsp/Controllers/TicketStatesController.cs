﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FirstProjectAsp.Models;

namespace FirstProjectAsp.Controllers
{
    public class TicketStatesController : Controller
    {
        private IssuesTrackingEntities db = new IssuesTrackingEntities();

        // GET: TicketStates
        public ActionResult Index()
        {
            return View(db.TicketStates.ToList());
        }

        // GET: TicketStates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketState ticketState = db.TicketStates.Find(id);
            if (ticketState == null)
            {
                return HttpNotFound();
            }
            return View(ticketState);
        }

        // GET: TicketStates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TicketStates/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,AreaName")] TicketState ticketState)
        {
            if (ModelState.IsValid)
            {
                db.TicketStates.Add(ticketState);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ticketState);
        }

        // GET: TicketStates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketState ticketState = db.TicketStates.Find(id);
            if (ticketState == null)
            {
                return HttpNotFound();
            }
            return View(ticketState);
        }

        // POST: TicketStates/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,AreaName")] TicketState ticketState)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticketState).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ticketState);
        }

        // GET: TicketStates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketState ticketState = db.TicketStates.Find(id);
            if (ticketState == null)
            {
                return HttpNotFound();
            }
            return View(ticketState);
        }

        // POST: TicketStates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TicketState ticketState = db.TicketStates.Find(id);
            db.TicketStates.Remove(ticketState);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
