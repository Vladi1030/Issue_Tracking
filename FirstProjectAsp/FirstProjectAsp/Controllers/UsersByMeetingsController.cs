﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FirstProjectAsp.Models;

namespace FirstProjectAsp.Controllers
{
    public class UsersByMeetingsController : Controller
    {
        private IssuesTrackingEntities db = new IssuesTrackingEntities();

        // GET: UsersByMeetings
        public ActionResult Index()
        {
            var usersByMeetings = db.UsersByMeetings.Include(u => u.Meeting1).Include(u => u.SystemUser);
            if (Session["UserID"] != null)
            {
                return View(usersByMeetings.ToList());
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        public ActionResult AssignedUsers(int? id)
        {
            var usersByMeetings = db.UsersByMeetings;
            foreach (UsersByMeeting user in usersByMeetings)
            {
                if (user.Meeting1.Id != id)
                {
                    usersByMeetings.Remove(user);
                }
            }
            if (Session["UserID"] != null)
            {
                return View(usersByMeetings.ToList());
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // GET: UsersByMeetings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsersByMeeting usersByMeeting = db.UsersByMeetings.Find(id);
            if (usersByMeeting == null)
            {
                return HttpNotFound();
            }
            if (Session["UserID"] != null)
            {
                return View(usersByMeeting);
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // GET: UsersByMeetings/Create
        public ActionResult Create()
        {
            ViewBag.Meeting = new SelectList(db.Meetings, "Id", "Title");
            ViewBag.AssignedUser = new SelectList(db.SystemUsers, "Id", "UserName");
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // POST: UsersByMeetings/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Meeting,AssignedUser")] UsersByMeeting usersByMeeting)
        {
            if (ModelState.IsValid)
            {
                db.UsersByMeetings.Add(usersByMeeting);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Meeting = new SelectList(db.Meetings, "Id", "Title", usersByMeeting.Meeting);
            ViewBag.AssignedUser = new SelectList(db.SystemUsers, "Id", "UserName", usersByMeeting.AssignedUser);
            if (Session["UserID"] != null)
            {
                return View(usersByMeeting);
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // GET: UsersByMeetings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsersByMeeting usersByMeeting = db.UsersByMeetings.Find(id);
            if (usersByMeeting == null)
            {
                return HttpNotFound();
            }
            ViewBag.Meeting = new SelectList(db.Meetings, "Id", "Title", usersByMeeting.Meeting);
            ViewBag.AssignedUser = new SelectList(db.SystemUsers, "Id", "UserName", usersByMeeting.AssignedUser);
            if (Session["UserID"] != null)
            {
                return View(usersByMeeting);
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // POST: UsersByMeetings/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Meeting,AssignedUser")] UsersByMeeting usersByMeeting)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usersByMeeting).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Meeting = new SelectList(db.Meetings, "Id", "Title", usersByMeeting.Meeting);
            ViewBag.AssignedUser = new SelectList(db.SystemUsers, "Id", "UserName", usersByMeeting.AssignedUser);
           
            if (Session["UserID"] != null)
            {
                return View(usersByMeeting);
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // GET: UsersByMeetings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsersByMeeting usersByMeeting = db.UsersByMeetings.Find(id);
            if (usersByMeeting == null)
            {
                return HttpNotFound();
            }
            if (Session["UserID"] != null)
            {
                return View(usersByMeeting);
            }
            else
            {
                return RedirectToAction("NotPer", "Message");
            }
        }

        // POST: UsersByMeetings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UsersByMeeting usersByMeeting = db.UsersByMeetings.Find(id);
            db.UsersByMeetings.Remove(usersByMeeting);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
